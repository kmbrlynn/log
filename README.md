# Log

Notes for myself, mostly

* [Bash cheatsheet](#8-bash-cheatsheet)
* [Mermaid diagram CLI tool](#7-mermaid)
* [Tiny syslogging lib](#6-clog)
* [Coding gifs](#5-gd)
* [Commander Genius](#4-keen)
* [Publishing notes to GitLab Pages](#3-gitlab-pages)
* [VSCode settings](#2-vscode)
* [Filtering git repos](#1-git-filter)
* [Hello GitLab](#0-hello)

-----

## [Bash cheatsheet](#){name=8-bash-cheatsheet}
**2024-02-13 Tuesday**

Tips & tricks if I'm someplace new
and an [alias](https://en.wikipedia.org/wiki/Sydney_Bristow) can't save me

<details>

#### Misc sysadmin

<details><summary>Directory size at a glance</summary>

Get the size of each dir in a dir

```shell
du -ch --max-depth=1
```

</details>

<details><summary>Eyeball clocks</summary>

Watch system and hardware clocks in the same [format](https://www.rfc-editor.org/info/rfc3339)
with high-ish precision. Sanity-check a gigantic ground truth: [utc.is](https://time.is/UTC)

```shell
watch -n0.1 "sudo hwclock; date --rfc-3339='ns'"
```
</details>

<details><summary>Quick useradd</summary>

Give `newkid` on the blk a home dir, sudo and serial privs

```shell
useradd -m -G sudo,dialout -s /bin/bash newkid
passwd newkid
# or go without passwd
echo "newkid ALL=(ALL) NOPASSWD:ALL" | sudo tee -a /etc/sudoers
sudo passwd --delete newkid
```
</details>

<details><summary>What's my WAN address?</summary>

```shell
curl ifconfig.me; echo
# no curl?
wget -O - ifconfig.me 2>/dev/null; echo
```

</details>

<details><summary>netcat</summary>

Listen on localhost:3000, send with a 1-second timeout

```shell
nc -l localhost 3000
# and in another terminal
echo "Hello, world!" | nc -w 1 localhost 3000
```
</details>

<details><summary>Dump systemd logs</summary>

Filter on a given time segment, spit it out raw with no journal timestamps

```shell
sudo journalctl -u my_unit --since 2024-02-10 --until yesterday -o cat
```

</details>

<details><summary>GNU screen</summary>

List screen sessions

```shell
screen -ls
No Sockets found in /run/screen/S-kmbrlynn.
```

Create a named session with a logfile, do whatever, then detach from it

```shell
screen -L -Logfile foo.log -S foo
while true; do date; sleep 1; done
Mon Apr 17 05:02:12 PM EDT 2023
Mon Apr 17 05:02:13 PM EDT 2023
Mon Apr 17 05:02:14 PM EDT 2023
Mon Apr 17 05:02:15 PM EDT 2023
# <Ctrl-a, d> to detach it (not k, which will kill it) 
# [detached from 1297589.foo]
```

Reattach to it

```shell
screen -r foo
# Now you'll keep seeing timestamps. <Ctrl-a, k> to actually kill this one
```

If you specify `foo.log` again, even after a session is killed, it'll
append to it rather than overwriting it.

</details>

#### Compiled stuff

<details><summary>gdb with args</summary>

Run `a.out` non-interactively with arbitrary number of args

```shell
gdb -ex=r --args ./a.out my-arg foo another:bar and baz
```

Or, interactively

```shell
gdb ./a.out
# (gdb) set args my-arg foo another:bar and baz
# (gdb) run
```

And with redirects

```shell
gdb ./a.out
# (gdb) set args my-arg foo another:bar and baz <file_input.txt
# (gdb) run
```

Helpful link [here](https://newbedev.com/how-to-pass-arguments-and-redirect-stdin-from-a-file-to-program-run-in-gdb)

</details>

<details><summary>Out-of-tree kernel modules</summary>

High-level process for compiling and installing an out-of-tree
Linux kernel module

```shell
cd /path/to/linux/source/tree
make prepare modules_prepare
ln -fs /path/to/linux/source/tree /lib/modules/$(uname -r)/build
ln -fs /path/to/linux/source/tree /lib/modules/$(uname -r)/source
cd /path/to/my_module_src
make -j$(nproc)
install -p -m 644 my_module.ko /lib/modules/$(uname -r)/kernel/<proper subdir>
depmod -a
modprobe my_module
lsmod
```

</details>

#### Python

<details><summary>virtualenvs</summary>

Create a virtual environment called 'venv':

```bash
# Go into the repo
cd repo

# Create a virtual environment using the venv command, also call it 'venv' per
# convention, this will create a venv directory that you should prob gitignore
python -m venv venv
```

Activate the virtualenv

```bash
source venv/bin/activate
# Now you're in it, safe to do whatever
(venv) $ pip install something-messy
(venv) $ deactivate
# Now you've dropped out of it
```

Tutorial [here](https://docs.python.org/3/tutorial/venv.html)

</details>

<details><summary>update-alternatives</summary>

If you're nervous about clobbering symlinks, try
[update-alternatives](https://linux.die.net/man/8/update-alternatives)

```shell
sudo update-alternatives --list python
# update-alternatives: error: no alternatives for python
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 1
# update-alternatives: using /usr/bin/python3 to provide /usr/bin/python (python) in auto mode
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 2
# update-alternatives: using /usr/bin/python2 to provide /usr/bin/python (python) in auto mode
sudo update-alternatives --list python
# /usr/bin/python2
# /usr/bin/python3
```

Interactively set the versioning

```shell
sudo update-alternatives --config python
# There are 2 choices for the alternative python (providing /usr/bin/python).
#
#  Selection    Path              Priority   Status
# ------------------------------------------------------------
# * 0            /usr/bin/python2   2         auto mode
#   1            /usr/bin/python2   2         manual mode
#   2            /usr/bin/python3   1         manual mode
#
# Press <enter> to keep the current choice[*], or type selection number: 2
# update-alternatives: using /usr/bin/python3 to provide /usr/bin/python (python) in manual mode
sudo update-alternatives --config python
# There are 2 choices for the alternative python (providing /usr/bin/python).
# 
#  Selection    Path              Priority   Status
# ------------------------------------------------------------
#   0            /usr/bin/python2   2         auto mode
#   1            /usr/bin/python2   2         manual mode
# * 2            /usr/bin/python3   1         manual mode
#
# Press <enter> to keep the current choice[*], or type selection number:
```

</details>

</details>

## [Mermaid diagram CLI tool: `mmdc`](#){name=7-mermaid}
**2022-10-05 Wednesday**

[Mermaid markdown diagramming](https://mermaid-js.github.io) is so wonderful!
But sometimes you can't rely on a web page to render these beautiful images for
you - maybe you need to convert them to SVG or PNG and share them some other way.

Fortunately, [mmdc](https://github.com/mermaid-js/mermaid-cli) is a Mermaid CLI
tool that can help us do that. This is how to install and use it.

<details>

#### Install

A [local install](https://github.com/mermaid-js/mermaid-cli#install-locally) is
recommended. Using Node, I just put it in my home directory:

```bash
cd ~
npm install @mermaid-js/mermaid-cli
```

Then I added `~/node_modules/.bin` to my `$PATH`, and now it's available:

```bash
mmdc --help
# Usage: mmdc [options]
# 
# Options:
#   -V, --version                                   output the version number
#   -t, --theme [theme]                             Theme of the chart, could be default, forest, dark or neutral. Optional. Default: default (default: "default")
#   -w, --width [width]                             Width of the page. Optional. Default: 800 (default: "800")
#   -H, --height [height]                           Height of the page. Optional. Default: 600 (default: "600")
#   -i, --input <input>                             Input mermaid file. Files ending in .md will be treated as Markdown and all charts (e.g. ```mermaid (...)```) will be extracted and generated. Required.
#   -o, --output [output]                           Output file. It should be either md, svg, png or pdf. Optional. Default: input + ".svg"
#   -e, --outputFormat <format>                     Output format for the generated image. It should be either svg, png or pdf. Optional. Default: output file extension
#   -b, --backgroundColor [backgroundColor]         Background color for pngs/svgs (not pdfs). Example: transparent, red, '#F0F0F0'. Optional. Default: white
#   -c, --configFile [configFile]                   JSON configuration file for mermaid. Optional
#   -C, --cssFile [cssFile]                         CSS file for the page. Optional
#   -s, --scale [scale]                             Puppeteer scale factor, default 1. Optional
#   -f, --pdfFit [pdfFit]                           Scale PDF to fit chart
#   -q, --quiet                                     Suppress log output
#   -p --puppeteerConfigFile [puppeteerConfigFile]  JSON configuration file for puppeteer. Optional
#   -h, --help                                      display help for command
```

#### The diagram

Cool, so let's draw a little mermaid graph right here in this README.md file:

```mermaid
graph LR
    A[home]
    B[grandma's]
    A -- over river, thru woods --> B
```

#### Convert to SVG or PNG

The graph above may or may not show up, depending on your site's
rendering capabilities. If it doesn't, here's how you can convert a chart
to an SVG file:

```bash
cat << EOF | mmdc -o river-and-woods.svg
>     graph LR
>         A[home]
>         B[grandma's]
>         A -- over river, thru woods --> B
> EOF
```

You don't have to use a heredoc if you don't want to, alternatively we generate
a Mermaid (note the `.mmd` extension) file and run mmdc more conventionally:

```bash
echo -e "graph LR\n\tA[home]\n\tB[grandma's]\n\tA -- over river, thru woods -->B" > /tmp/river-and-woods.mmd
cat /tmp/river-and-woods.mmd
graph LR
        A[home]
        B[grandma's]
        A -- over river, thru woods -->B
mmdc -i /tmp/river-and-woods.mmd -o river-and-woods.svg
```

Either method puts the image version where our site can access it, and we can
render it in markdown the normal way:

![](river-and-woods.svg)

It works for PNG as well, just change the file extension:

```bash
mmdc -i /tmp/river-and-woods.mmd -o river-and-woods.png
```

And if we put out output file where the GitLab server can find it, it'll render

```bash
mv river-and-woods.svg public/
./markdown2html.sh
```

Neat!

</details>

## [Tiny syslogging lib: `clog`](#){name=6-clog}
**2022-01-19 Wednesday**

I've been thinking about [CVE-2021-44228](https://cve.mitre.org/cgi-bin/cvename.cgi?name=2021-44228)
today, and logging systems more generally. There is some good logging-level
background discussion to be found on
[this Stack Overflow thread](https://stackoverflow.com/questions/2031163/when-to-use-the-different-log-levels).

<details>

Here is a [simple logging module](https://gitlab.com/kmbrlynn/clog) that can
be linked to any C appplication for ease of clogging your life with even more
ansi-stylized files. Use it in your code like so:

```c
fprintlog(stderr, INFO, "Registering stop at intersection %d of %d\n", 5, 37);
```

The current IS0 8601 date string and the Severity Level per
[RFC 5424](https://datatracker.ietf.org/doc/html/rfc5424#section-6.2.1)
are printed to the console. Running the included test app, for example, prints:

<pre>
2022-01-19T23:21:55 [ <span style="font-weight:bold;color:red;">EMERG </span> ] Engine falling out!!
2022-01-19T23:21:55 [ <span style="font-weight:bold;color:olive;">ALERT </span> ] Brakes malfunctioning!
2022-01-19T23:21:55 [ <span style="color:red;">CRIT  </span> ] Fuel tank nearly empty
2022-01-19T23:21:55 [ <span style="color:olive;">ERROR </span> ] Windshield wipers unresponsive
2022-01-19T23:21:55 [ <span style="color:purple;">WARN  </span> ] Road conditions becoming icy
2022-01-19T23:21:55 [ <span style="color:teal;">NOTICE</span> ] Journey has concluded
2022-01-19T23:21:55 [ <span style="color:blue;">INFO  </span> ] Registering stop at intersection 5 of 37
2022-01-19T23:21:55 [ <span style="color:green;">DEBUG </span> ] Accel average +0.621040 m/s in last 5min
</pre>

By the way, the code block above isn't a code block, and since I ain't no frontend
expert, the html/css tags required to render the colors were obtained the lazy
way: by piping test app output into the wonderful [aha](https://github.com/theZiz/aha)
utility:

```bash
sudo apt install aha
./test 2>&1 | aha | grep 2022 -B 1 -A 1
# <pre>
# 2022-01-19T23:26:08 [ <span style="font-weight:bold;color:red;">EMERG </span> ] Engine falling out!!
# 2022-01-19T23:26:08 [ <span style="font-weight:bold;color:olive;">ALERT </span> ] Brakes malfunctioning!
# 2022-01-19T23:26:08 [ <span style="color:red;">CRIT  </span> ] Fuel tank nearly empty
# 2022-01-19T23:26:08 [ <span style="color:olive;">ERROR </span> ] Windshield wipers unresponsive
# 2022-01-19T23:26:08 [ <span style="color:purple;">WARN  </span> ] Road conditions becoming icy
# 2022-01-19T23:26:08 [ <span style="color:teal;">NOTICE</span> ] Journey has concluded
# 2022-01-19T23:26:08 [ <span style="color:blue;">INFO  </span> ] Registering stop at intersection 5 of 37
# 2022-01-19T23:26:08 [ <span style="color:green;">DEBUG </span> ] Accel average +0.621040 m/s in last 5min
# </pre>
```

</details>

## [Building the `gd` graphics library's gif example](#){name=5-gd}
**2021-12-28 Tuesday**

The [`gd`](https://libgd.github.io) graphics library can be used for
programmatic image creation. Here's an example of how to use the library to
create an animated gif in C

<details>

Install `gd` development tools

```bash
sudo apt install libgd-dev
```

Grab the project's example application for your programming language of choice.
I chose the C example:

```bash
curl -OL https://raw.githubusercontent.com/libgd/libgd/master/examples/gif.c
```

Compile the exampe, linking to the gd library

```bash
gcc gif.c -o gif -lgd
```

Run the program

```bash
./gif
# (163, 151, 162)
# (252, 249, 121)
# (233, 226, 45)
# (8, 87, 39)
# (82, 130, 119)
# (237, 88, 61)
# (36, 74, 104)
# (60, 53, 181)
# (17, 50, 61)
# (207, 206, 35)
# (249, 150, 252)
# (14, 123, 140)
# (188, 159, 123)
# (135, 236, 138)
# (99, 14, 60)
# (6, 32, 167)
# (76, 245, 236)
# (104, 160, 9)
# (201, 65, 219)
# (243, 81, 5)
```

View output in your favorite gif-viewer

```bash
firefox anim.gif
```
</details>

## [Commander Genius](#){name=4-keen}
**2021-12-24 Friday**

Building [Commander Genius](https://clonekeenplus.sourceforge.io/), a port of
the beloved 1990s shareware game, [Commander Keen](https://en.wikipedia.org/wiki/Commander_Keen)

<details>

#### Install dependencies

I had most of these already, but `cmake` needed to be added to the list

```bash
sudo apt install build-essential libgl1-mesa-dev libcurl4-openssl-dev \
    zlib1g-dev libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev \
    cmake
```

#### Fork the repo, clone, and build

This didn't take as long as I thought it would!

```bash
git clone git@gitlab.com:kmbrlynn/Commander-Genius.git
mkdir CGeniusBuild && cd CGeniusBuild
cmake ../Commander-Genius
make -j
```

#### Run

The game binary compiles to `CGenisuBuild/src/CGeniusExe` - run it, and follow
the prompts to download the game data. I chose Keen 1, "Invasion of the
Vorticons," and by default the game data was put here:

```bash
ls ~/.CommanderGenius/games/Keen\ 1/keen1/
# EGAHEAD.CK1   LEVEL01.CK1  LEVEL08.CK1  LEVEL15.CK1   PREVIEW3.CK1
# EGALATCH.CK1  LEVEL02.CK1  LEVEL09.CK1  LEVEL16.CK1   preview.bmp
# EGASPRIT.CK1  LEVEL03.CK1  LEVEL10.CK1  LEVEL80.CK1   PREVIEWS.CK1
# ENDTEXT.CK1   LEVEL04.CK1  LEVEL11.CK1  LEVEL81.CK1   SCORES.CK1
# FINALE.CK1    LEVEL05.CK1  LEVEL12.CK1  LEVEL90.CK1   SOUNDS.CK1
# HELPTEXT.CK1  LEVEL06.CK1  LEVEL13.CK1  ORDER.FRM     STORYTXT.CK1
# KEEN1.EXE     LEVEL07.CK1  LEVEL14.CK1  PREVIEW2.CK1  VENDOR.DOC
```

Happy pogo-sticking!

Alternatively, play Vorticons in the browser, via DOSBox,
[on archive dot org](https://archive.org/details/commander_keen_volume_one_131)

</details>

## [Publishing notes to GitLab Pages](#){name=3-gitlab-pages}
**2021-12-24 Friday**

For several years now I have kept one big `notes.md` file on my computer, for
working out some pseudocode or my grocery list or tracking that bash command
that's always so hard to remember - so greppable!

Why not publish these to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
for a quick an easy reference?

<details>

#### Use plain html instead of a static site generator

SSGs introduce overhead that defeats the point of my planned edit-commit-push
workflow. So I just used the [`plain-html`](https://gitlab.com/pages/plain-html)
method.

Right now, log notes are kept at [log/README.md](https://gitlab.com/kmbrlynn/log/-/blob/master/README.md).
That's literally it. It already renders them pretty nicely, particularly with the
content expansion through the [html `<details>` tag](https://www.w3schools.com/tags/tag_details.asp).

To essentially replicate this on Pages, we only need to add a handful of things:
a `public` directory with a keepfile, a gitignore file, the CI yaml, and a bash
script:

```bash
cd log
mkdir public
echo "public/index.html" > .gitignore
touch public/.keep
touch .gitlab-ci.yml
touch markdown2html.sh && chmod +x markdown2html.sh
```

#### The `.gitlab-ci.yml` file

It's slightly different from the one provided in the plain html demo:

```yml
image: ubuntu:20.04

pages:
  stage: deploy
  script:
  - DEBIAN_FRONTEND=noninteractive apt update && apt install --no-install-recommends -yy pandoc
  - ./markdown2html.sh
  artifacts:
    paths:
    - public
    expire_in: 1 day
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
```

Notably, I changed the CI image from Alpine Linux to Ubuntu 20.04 in order to
get a straightforward way of installing [`pandoc`](https://pandoc.org). In the
future, the lighter-weight Alpine distro is probably preferable.

#### The `markdown2html.sh` script

This is really just so stupid.

```bash
set -euo pipefail

CSS="https://unpkg.com/sakura.css/css/sakura.css"

echo "<!DOCTYPE html>" > public/index.html
echo "<link rel=\"stylesheet\" href=\"${CSS}\" />" >> public/index.html

pandoc README.md -o /tmp/index.html && cat /tmp/index.html >> public/index.html

echo "</html>" >> public/index.html
```

It took me a while to find a nice clean classless CSS design, but I came across
[Sakura](https://oxal.org/projects/sakura/) and really liked it.

#### Publishing notes is simple

1. Add the cool new thing to `README.md`
2. Optionally run `./markdown2html.sh` to verify that `index.html` looks ok
   in a browser locally
3. Commit and push

That's all, folks! Merry Christmas Eve.

</details>

## [VSCode settings](#){name=2-vscode}
**2021-12-23 Thursday**

Sometimes the [VSCode](https://code.visualstudio.com/) settings GUI is
difficult to use, or you need to put in _just-so_ search strings to even find
the right one, and it's annoying. In these cases I find it easier to just
edit the settings JSON directly.

<details>

#### The `settings.json` file

If you want, for example, a vertical column in your VSCode IDE at 80 characters,
add this field to your `settings.json` file:

```json
"editor.rulers": [80]
```

Or make whitespace characters visible with little faint dots:

```json
"editor.renderWhitespace": "all"
```

YMMV, but I applied this to the user-wide settings, with a default Debian-flavor
install via `apt install code`, which places the file here:

```bash
cat ${HOME}/.config/Code/User/settings.json
{
    "window.zoomLevel": -2,
    "editor.rulers": [80],
    "editor.renderWhitespace": "all"
}
```

[This StackOverflow post](https://stackoverflow.com/questions/29968499/vertical-rulers-in-visual-studio-code)
was helpful, [and this one](https://stackoverflow.com/questions/30140595/show-whitespace-characters-in-visual-studio-code)

#### Other things I like to add

Extensions!

- [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
- [C/C++ IntelliSense](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)

</details>

## [Filtering git repos](#){name=1-git-filter}
**2021-01-31 Sunday**

Is your git history a mess that needs filtering?
[`git-filter-repo`](https://github.com/newren/git-filter-repo) might help.
Here's an example of how to change things like your commit email.

<details>

Something you might want to do is redact sensitive data that has been commited
to a git history without destroying the rest of the git history - for example,
replacing commits that say `Author: kmbrlynn <kmbrlynn@discreet-email-address.com>`
with `Author: kmbrlynn <kmbrlynn@users.noreply.some-git-server.com>`

It looks like there's a (probably dangerous!) tool to do this, written by
newren: [`git-filter-repo`](https://github.com/newren/git-filter-repo)

#### The problem

Let's take the `demo-repo` repo as an example, since it's only got three commits.

```bash
git clone https://some-git-server.com/kmbrlynn/demo-repo.git
cd demo-repo
```

Note that two of these commits have the offending email address:

```bash
git log
# commit 727ed1b8d9b29871847eb75e41fe5575286ec2de (HEAD -> master, origin/master, origin/HEAD)
# Merge: 6232a65 b9783f1
# Author: kmbrlynn <kmbrlynn@discreet-email-address.com>
# Date:   Sat Nov 22 09:23:28 2014 -0500
# 
#     Merge https://some-git-server.com/kmbrlynn/demo-repo
# 
# commit 6232a65d831f5d418b531ad16f597c1eb51a99e3
# Author: kmbrlynn <kmbrlynn@discreet-email-address.com>
# Date:   Sat Nov 22 09:23:02 2014 -0500
# 
#     initial
# 
# commit b9783f1a9477e82fa56e4fef91e4ccb7243057ed
# Author: kmbrlynn <kmbrlynn@users.noreply.some-git-server.com>
# Date:   Sat Nov 22 09:19:58 2014 -0500
# 
#     Initial commit
```

#### Find a tool to help: `git-filter-repo`

Let's install the `git-filter-repo` python utility to rectify this.
Per the [INSTALL.md](https://github.com/newren/git-filter-repo/blob/main/README.md)
documentation, pip is recommended. Let's first make sure pip will install stuff
in a place that's already accessible to our `$PATH`- otherwise packages
will get installed to something like `${HOME}/.local/lib/python3.8/site-packages`,
which is annoying. 

```bash
pip3 config set global.target /usr/local/bin
```

Now install this git history editor, made by newren

```bash
sudo pip3 install git-filter-repo
```

#### The solution

Now run the filtering command to replace sensitive emails

```bash
git filter-repo --email-callback \
    'return email.replace(b"kmbrlynn@discreet-email-address.com", \
    b"kmbrlynn@users.noreply.some-git-server.com")'
# Parsed 3 commits
# New history written in 0.08 seconds; now repacking/cleaning...
# Repacking your repo and cleaning out old unneeded objects
# HEAD is now at 16a0dd2 Merge https://some-git-server.com/kmbrlynn/demo-repo
# Enumerating objects: 21, done.
# Counting objects: 100% (21/21), done.
# Delta compression using up to 4 threads
# Compressing objects: 100% (19/19), done.
# Writing objects: 100% (21/21), done.
# Total 21 (delta 6), reused 0 (delta 0)
# Completely finished after 0.13 seconds.
```

Check out the git log now:

```bash
git log
# commit 16a0dd288a8c0c2ed25cd8e751e1f32298894659 (HEAD -> master)
# Merge: 5f10ccc b9783f1
# Author: kmbrlynn <kmbrlynn@users.noreply.some-git-server.com>
# Date:   Sat Nov 22 09:23:28 2014 -0500
# 
#     Merge https://some-git-server.com/kmbrlynn/demo-repo
# 
# commit 5f10ccc67b60ea8f981a4dd9ac33bf94ab4a2c35
# Author: kmbrlynn <kmbrlynn@users.noreply.some-git-server.com>
# Date:   Sat Nov 22 09:23:02 2014 -0500
# 
#     initial
# 
# commit b9783f1a9477e82fa56e4fef91e4ccb7243057ed
# Author: kmbrlynn <kmbrlynn@users.noreply.some-git-server.com>
# Date:   Sat Nov 22 09:19:58 2014 -0500
# 
#     Initial commit
```

Fixed! Now do this for all of them.

```bash
cd repos_to_clean_up
for f in `ls`; do
    pushd $f
    git filter-repo --email-callback \
        'return email.replace(b"kmbrlynn@discreet-email-address.com", \
        b"kmbrlynn@users.noreply.some-git-server.com")'
    popd
done
```

</details>

## [Hello, GitLab!](#){name=0-hello}
**2021-01-30 Saturday**

Back at it. Jotted some notes on how to get set up in
[GitLab](https://docs.gitlab.com/ee/user/project/repository/)

<details>

Funny thing about finding the thing you love. You take a bunch of wrong
but enjoyable turns in the form of costly tuition in search of it.
Then you go back to school for it. You work really hard, you have
a bunch of support and luck. You land an internship, and then a job,
and then an even better job.... and one day you wake up and realize
that long ago you lost all the time and brainspace that had afforded you
the ability to do it for fun.

Well, I'm hoping that will change for me this year. Burnout is real!
It is important to me to jump back into the lovable, messy world of
open source. Creating a fresh GitLab account seemed like a good place to start.

#### Server setup

Generate a new SSH keypair on your local machine

```bash
ssh-keygen -t ed25519 -C "kmbrlynn-gitlab"
```

Per instructions on [Profile > User Settings > SSH Keys](https://gitlab.com/-/profile/keys),
place the public key in the web GUI box. Then, still in the GitLab GUI,
create a new empty public repo called `uml` 

#### Local setup

Clone and `cd` into the empty repo you just created.

```bash
git clone git@gitlab.com:kmbrlynn/uml.git
cd uml
```

Note that the initial commit was created through the web gui when I
elected to initialize the repo with a readme

```bash
git log
# commit 25bc92ee85725d6acead5530a0576036bc6caddb (HEAD -> master, origin/master, origin/HEAD)
# Author: kmbrlynn <7799686-kmbrlynn@users.noreply.gitlab.com>
# Date:   Sat Jan 30 19:58:04 2021 +0000
# 
#     Initial commit
```

Set up your local machine so it has a global git config pointing to
your new GitLab account, 

```bash
git config --global user.name "kmbrlynn"
git config --global user.email "7799686-kmbrlynn@users.noreply.gitlab.com"
```

and do a test commit ensuring these credentials are utilized. Looks good.

```bash
git log
# commit b5e39d9704ee6af1c57e89be91a959ce5fcb8435 (HEAD -> master)
# Author: kmbrlynn <7799686-kmbrlynn@users.noreply.gitlab.com>
# Date:   Sat Jan 30 15:16:18 2021 -0500
# 
#     test commit
# 
# commit 25bc92ee85725d6acead5530a0576036bc6caddb (origin/master, origin/HEAD)
# Author: kmbrlynn <7799686-kmbrlynn@users.noreply.gitlab.com>
# Date:   Sat Jan 30 19:58:04 2021 +0000
# 
#     Initial commit
```

Remotes will work by default, since this was created in the GitLab GUI.

```bash
git remote -v
origin  git@gitlab.com:kmbrlynn/uml.git (fetch)
origin  git@gitlab.com:kmbrlynn/uml.git (push)
```

Push changes back up

```bash
git push
```

All set! Let's try it from the other direction.

#### Local setup the other way

Create a local project. This one doesn't exist on GitLab yet.
Initialize it as a `git` repo - this one will just be a collection
of notes for myself.

```bash
mkdir log
cd log
git init
```

Check out to a new default master branch, and commit something

```bash
git checkout -b master
echo "Notes for myself, mostly" > README.md
git add README.md
git commit -m "initial commit"
```

Now we are ready to push the existing project up. First create
an empty `log` repo in the GitLab GUI, so we have somewhere
to push to. Then, add it as a remote. 

```bash
git remote add origin git@gitlab.com:kmbrlynn/log.git
git remote -v
origin  git@gitlab.com:kmbrlynn/log.git (fetch)
origin  git@gitlab.com:kmbrlynn/log.git (push)
```

And push master (setting the branch's remote upstream since this is
the first time we are pushing).

```bash
git push --set-upstream origin master
```

As you can see, it's quicker and easier to start with the repo
on the server (which automates a lot of stuff), then pull it down.

</details>
