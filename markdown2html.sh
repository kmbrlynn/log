set -euo pipefail

CSS="https://cdn.jsdelivr.net/gh/yegor256/tacit@gh-pages/tacit-css-1.5.0.min.css"
SYNTAX_CSS="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/styles/default.min.css"
SYNTAX_SCRIPT="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/11.9.0/highlight.min.js"

echo "<!DOCTYPE html>" > public/index.html
echo "<link rel=\"stylesheet\" href=\"${CSS}\" />" >> public/index.html
echo "<link rel=\"stylesheet\" href=\"${SYNTAX_CSS}\" />" >> public/index.html
echo "<script src=\"${SYNTAX_SCRIPT}\"></script>" >> public/index.html
echo "<script>hljs.highlightAll();</script>" >> public/index.html

pandoc README.md -o /tmp/index.html && cat /tmp/index.html >> public/index.html

echo "</html>" >> public/index.html
